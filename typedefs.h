/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           typedefs.h
 *  Purpose:            Various typedef defines
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/


#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_

#include <string.h>

//
// Generic linux functions
//
#define GEN_PRINTF               printf
#define GEN_SPRINTF              sprintf
#define GEN_SNPRINTF             snprintf
#define GEN_FPRINTF              fprintf
#define GEN_VPRINTF              vprintf
#define GEN_VFPRINTF             vfprintf
#define GEN_VSNPRINTF            vsnprintf
#define GEN_FSEEK                fseek
#define GEN_FFLUSH               fflush
#define GEN_SSCANF               sscanf
#define GEN_STRCMP               strcmp
#define GEN_STRNCMP              strncmp
#define GEN_STRCMPI              strcasecmp
#define GEN_STRNCMPI             strncasecmp
#define GEN_STRCAT               strcat
#define GEN_STRNCAT              strncat
#define GEN_STRCPY               strcpy
#define GEN_STRNCPY              strncpy
#define GEN_STRLEN               strlen
#define GEN_STRSTR               strstr
#define GEN_STRCHR               strchr
#define GEN_STRSTRI              strcasestr
#define GEN_MEMSET               memset
#define GEN_MEMCPY               memcpy
#define GEN_MEMCMP               memcmp

#ifndef TYPEDEF_INT8
   typedef signed char     int8;
   #define TYPEDEF_INT8
#endif

#ifndef TYPEDEF_U_INT8
   typedef unsigned char   u_int8;
   #define TYPEDEF_U_INT8
#endif

#ifndef TYPEDEF_INT16
   typedef signed short    int16;
   #define TYPEDEF_INT16
#endif

#ifndef TYPEDEF_U_INT16
   typedef unsigned short  u_int16;
   #define TYPEDEF_U_INT16
#endif

#ifndef TYPEDEF_INT32
   typedef signed long     int32;
   #define TYPEDEF_INT32
#endif

#ifndef TYPEDEF_U_INT32
   typedef unsigned long   u_int32;
   #define TYPEDEF_U_INT32
#endif

#ifndef TYPEDEF_U_INT64
   typedef unsigned long long  u_int64;
   #define TYPEDEF_U_INT64
#endif

#ifndef TYPEDEF_INT64
   typedef signed long long  int64;
   #define TYPEDEF_INT64
#endif

//
// Typedefs for FAN Hat
//
typedef unsigned char   UBYTE;
typedef unsigned short  UWORD;
typedef unsigned long   ULONG;
typedef u_int8        (*PTRFUNC)(u_int8);
typedef void          (*PTRBYTE)(u_int16);

/*----------------------------------------------------------------------------
*        The following definitions are zero, non-zero based                  *
*---------------------------------------------------------------------------*/
#ifndef  TRUE
 #define FALSE                0
 #define TRUE                 (!FALSE)
#endif

/*----------------------------------------------------------------------------
*        Exit error codes
*---------------------------------------------------------------------------*/
#define  EXIT_CC_OKEE         0
#define  EXIT_CC_GEN_ERROR    1
#define  EXIT_CC_EXE_ERROR    126
#define  EXIT_CC_FND_ERROR    127
#define  EXIT_CC_ARG_ERROR    128
#define  EXIT_CC_MAX_ERROR    255

/*----------------------------------------------------------------------------
*                 UNIVERSAL "C" DECLARATIONS                                 *
*---------------------------------------------------------------------------*/
#define FOREVER               for (;;)

#ifndef NULL
#define NULL                  0
#endif

#endif   //_TYPEDEFS_H_
