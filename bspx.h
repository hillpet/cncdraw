/*  (c) Copyright:  2010..2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           bspx.h
 *  Purpose:            Board support header file porting layer for Arduino type AccelStepper.cpp
 *  Compiler/Assembler: NMake gcc build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Apr 2022:      Ported from TeensyCnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef  _BSPX_H_
#define  _BSPX_H_

//-----------------------------------------------------------------------------
// misc defines
//-----------------------------------------------------------------------------
#define BIT0                        0x01
#define BIT1                        0x02
#define BIT2                        0x04
#define BIT3                        0x08
#define BIT4                        0x10
#define BIT5                        0x20
#define BIT6                        0x40
#define BIT7                        0x80

#if defined(__RPI_PICO_RP2040__)
//-----------------------------------------------------------------------------
// Raspberry Pi Pico RP4020
// CNC XYZ axis
//-----------------------------------------------------------------------------
#define CNC_XAX_STP_INIT()
#define CNC_XAX_DIR_INIT()
#define CNC_XAX_LM1_INIT()
#define CNC_XAX_LM2_INIT()
//
#define CNC_XAX_MODE_STP(a)         
#define CNC_XAX_STP_0()             
#define CNC_XAX_STP_1()             
#define CNC_XAX_STP(a)              
#define CNC_XAX_MODE_DIR(a)         
#define CNC_XAX_DIR(a)              
#define CNC_XAX_MODE_MIN(a)         
#define CNC_XAX_MIN()               
#define CNC_XAX_MODE_MAX(a)         
#define CNC_XAX_MAX()               
//
#define CNC_YAX_STP_INIT()
#define CNC_YAX_DIR_INIT()
#define CNC_YAX_LM1_INIT()
#define CNC_YAX_LM2_INIT()
//
#define CNC_YAX_MODE_STP(a)         
#define CNC_YAX_STP_0()             
#define CNC_YAX_STP_1()             
#define CNC_YAX_STP(a)              
#define CNC_YAX_MODE_DIR(a)         
#define CNC_YAX_DIR(a)              
#define CNC_YAX_MODE_MIN(a)         
#define CNC_YAX_MIN()               
#define CNC_YAX_MODE_MAX(a)         
#define CNC_YAX_MAX()               
//
#define CNC_ZAX_STP_INIT()
#define CNC_ZAX_DIR_INIT()
#define CNC_ZAX_LM1_INIT()
#define CNC_ZAX_LM2_INIT()
//
#define CNC_ZAX_MODE_STP(a)         
#define CNC_ZAX_STP_0()             
#define CNC_ZAX_STP_1()             
#define CNC_ZAX_STP(a)              
#define CNC_ZAX_MODE_DIR(a)         
#define CNC_ZAX_DIR(a)              
#define CNC_ZAX_MODE_MIN(a)         
#define CNC_ZAX_MIN()               
#define CNC_ZAX_MODE_MAX(a)         
#define CNC_ZAX_MAX()               

#elif defined(__MAKER_PI_RP2040__)
//-----------------------------------------------------------------------------
// MAKER PI Raspberry Pi Pico RP4020
// GPIO Usage
//-----------------------------------------------------------------------------
#define GPIO_LED_HBT                25
//
#define GPIO_KEY_0                  20
#define GPIO_KEY_1                  21
#define GPIO_KEY_2                  22
//
#define GPIO_XAX_LM1                6
#define GPIO_XAX_LM2                7
#define GPIO_XAX_STP                0
#define GPIO_XAX_DIR                1
//
#define GPIO_YAX_LM1                8
#define GPIO_YAX_LM2                9
#define GPIO_YAX_STP                2
#define GPIO_YAX_DIR                3
//
#define GPIO_ZAX_LM1                26
#define GPIO_ZAX_LM2                27
#define GPIO_ZAX_STP                4
#define GPIO_ZAX_DIR                5
//
//      CNC X axis
#define CNC_XAX_MODE_STP(a)         (a ? gpio_set_dir(GPIO_XAX_STP,GPIO_OUT) : gpio_set_dir(GPIO_XAX_STP,GPIO_IN))
#define CNC_XAX_MODE_DIR(a)         (a ? gpio_set_dir(GPIO_XAX_DIR,GPIO_OUT) : gpio_set_dir(GPIO_XAX_DIR,GPIO_IN))
#define CNC_XAX_MODE_MIN(a)         (a ? gpio_set_dir(GPIO_XAX_LM1,GPIO_OUT) : gpio_set_dir(GPIO_XAX_LM1,GPIO_IN))
#define CNC_XAX_MODE_MAX(a)         (a ? gpio_set_dir(GPIO_XAX_LM2,GPIO_OUT) : gpio_set_dir(GPIO_XAX_LM2,GPIO_IN))
//
#define CNC_XAX_STP_0()             gpio_put(GPIO_XAX_STP,0)
#define CNC_XAX_STP_1()             gpio_put(GPIO_XAX_STP,1)
#define CNC_XAX_STP(a)              gpio_put(GPIO_XAX_STP,a)
#define CNC_XAX_DIR(a)              gpio_put(GPIO_XAX_DIR,a)
#define CNC_XAX_MIN()               gpio_get(GPIO_XAX_LM1)
#define CNC_XAX_MAX()               gpio_get(GPIO_XAX_LM2)
//
//      CNC Y axis
#define CNC_YAX_MODE_STP(a)         (a ? gpio_set_dir(GPIO_YAX_STP,GPIO_OUT) : gpio_set_dir(GPIO_YAX_STP,GPIO_IN))
#define CNC_YAX_MODE_DIR(a)         (a ? gpio_set_dir(GPIO_YAX_DIR,GPIO_OUT) : gpio_set_dir(GPIO_YAX_DIR,GPIO_IN))
#define CNC_YAX_MODE_MIN(a)         (a ? gpio_set_dir(GPIO_YAX_LM1,GPIO_OUT) : gpio_set_dir(GPIO_YAX_LM1,GPIO_IN))
#define CNC_YAX_MODE_MAX(a)         (a ? gpio_set_dir(GPIO_YAX_LM2,GPIO_OUT) : gpio_set_dir(GPIO_YAX_LM2,GPIO_IN))
//
#define CNC_YAX_STP_0()             gpio_put(GPIO_YAX_STP,0)
#define CNC_YAX_STP_1()             gpio_put(GPIO_YAX_STP,1)
#define CNC_YAX_STP(a)              gpio_put(GPIO_YAX_STP,a)
#define CNC_YAX_DIR(a)              gpio_put(GPIO_YAX_DIR,a)
#define CNC_YAX_MIN()               gpio_get(GPIO_YAX_LM1)
#define CNC_YAX_MAX()               gpio_get(GPIO_YAX_LM2)
//
//      CNC Z axis
#define CNC_ZAX_MODE_STP(a)         (a ? gpio_set_dir(GPIO_ZAX_STP,GPIO_OUT) : gpio_set_dir(GPIO_ZAX_STP,GPIO_IN))
#define CNC_ZAX_MODE_DIR(a)         (a ? gpio_set_dir(GPIO_ZAX_DIR,GPIO_OUT) : gpio_set_dir(GPIO_ZAX_DIR,GPIO_IN))
#define CNC_ZAX_MODE_MIN(a)         (a ? gpio_set_dir(GPIO_ZAX_LM1,GPIO_OUT) : gpio_set_dir(GPIO_ZAX_LM1,GPIO_IN))
#define CNC_ZAX_MODE_MAX(a)         (a ? gpio_set_dir(GPIO_ZAX_LM2,GPIO_OUT)  : gpio_set_dir(GPIO_ZAX_LM2,GPIO_IN))
//
#define CNC_ZAX_STP_0()             gpio_put(GPIO_ZAX_STP,0)
#define CNC_ZAX_STP_1()             gpio_put(GPIO_ZAX_STP,1)
#define CNC_ZAX_STP(a)              gpio_put(GPIO_ZAX_STP,a)
#define CNC_ZAX_DIR(a)              gpio_put(GPIO_ZAX_DIR,a)
#define CNC_ZAX_MIN()               gpio_get(GPIO_ZAX_LM1)
#define CNC_ZAX_MAX()               gpio_get(GPIO_ZAX_LM2)
     
#else
//=============================================================================
// NO recognized AT chip
//=============================================================================
#error   NO board support package defined
#endif


#endif   // _BSPX_H_
