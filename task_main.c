/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           task_main.c
 *  Purpose:            RPi-Pico task
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pico/stdlib.h"
//
#include "typedefs.h"
#include "config.h"

#define  DEFINE_GLOBALS
#include "globals.h"
#include "timer.h"
#include "bspx.h"
#include "task_main.h"
//
#define USE_PRINTF
#include "printx.h"

//
// Keep track of rudimentary time
//
static u_int8  ubHrs          = 0;              // RTC
static u_int8  ubMin          = 0;              // 
static u_int8  ubSec          = 0;              // 
//
static u_int8  main_UpdateTime            (u_int16);
static void    main_ShowTime              (void);

/* ====== Functions separator ===========================================
void ____Global_functions____(){}
=========================================================================*/

//
//  Function:  TASK_Main
//  Purpose:   Called every second for time keeping. 
//  Parms:     state
//
//  Returns:   new state
//
u_int8 TASK_Main(u_int8 ubState)
{
   switch(ubState)
   {
      case 0:
         //
         // Init:
         //    o Initialize all variables
         //    o Setup sampling
         //
         PRINTF("TASK_Main():Init" CRLF);
         ubHrs   = ubMin = ubSec = 0;
         ubState = 1;
         break;

      case 1:
         main_ShowTime();
         break;

      default:
         break;
   }
   //
   // Update global Hrs:Mins:Secs with 1 second.
   //
   if (main_UpdateTime(1) )
   {
      //
      // 15 minutes time-out: do something ?
      //
   }
   return(ubState);
}

/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/

//
//  Function:  main_UpdateTime
//  Purpose:   Handle the rudimentary time/date update scheme with 1 second
//  Parms:     Tick count (to assemble 1 Sec)
//
//  Updates    ubHrs:ubMins:ubSecs 00:00:00 ... 23:59:59
//  Returns:   1 on every ~15 mins chime
//
static u_int8 main_UpdateTime(u_int16 usSecs)
{
   u_int8 ubChime = 0;

   usSecs += (u_int16)ubSec;
   //
   while(usSecs > 59)
   {
      usSecs -= 60;
      ubMin++;
      if( (ubMin % 15) == 0 )
      {  
         // 15 mins: Chime the bells
         ubChime = 1;
      }
      if(ubMin == 60)
      {
         ubMin = 0;
         ubHrs++;
         if(ubHrs == 24)
         {
            ubHrs = 0;
         }
      }
   }
   ubSec = (u_int8)usSecs;
   return(ubChime);
}

//
//  Function:  main_ShowTime
//  Purpose:   Display elapsed time
//  Parms:     void
//
//  Returns:   
//
static void main_ShowTime(void)
{
   PRINTF("TASK_Main():Time=%02d:%02d:%02d" CRLF, ubHrs, ubMin, ubSec);
}

