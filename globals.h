/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           globals.h
 *  Purpose:            RPi-Pico global data header file
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#ifdef DEFINE_GLOBALS
   #define EXTERN
#else
   #define EXTERN extern
#endif
//
EXTERN u_int8  ubKey0;
EXTERN u_int8  ubKey1;
EXTERN u_int8  ubKey2;

//
// Cooperative scheduler struct
//
typedef struct SCHED
{
   u_int8  *pubState;
   u_int8 (*pfFunc)(u_int8);
}  SCHED;
//
// CNC Struct
//
typedef struct CNC
{
   u_int8  *pubState;
   u_int8 (*pfMode)(u_int8);
   u_int8 (*pfInOut)(u_int8);
}  CNC;

#endif  // _GLOBALS_H_
