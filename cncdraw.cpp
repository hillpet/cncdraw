/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           cncdraw.cpp
 *  Purpose:            Simple cooperative scheduler for RPi-Pico cncdraw project
 *  Compiler/Assembler: NMake gcc build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *    09 Apr 2022:      Move to C++
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <cstdint>

extern "C" 
{
   #include <stdio.h>
   #include <string.h>
   #include <stdlib.h>
   #include "pico/stdlib.h"
   #include "bsp/board.h"
   //
   #include "typedefs.h"
   #include "config.h"
   #include "globals.h"
   #include "timer.h"
   #include "bspx.h"
   #include "task_main.h"
   #include "task_led.h"
   #include "task_key.h"
   //
   #define USE_PRINTF
   #include "printx.h"
}
#include "AccelStepper.h"

//
// Prototypes of local funcs
//
static void    pico_SetupCnc        (int);
static void    pico_SetupIo         (void);
static void    pico_SetupUsb        (void);

//
#ifdef   FEATURE_MONITOR_STACK
static   u_int8  task_monitor       (u_int8);
u_int8   ubTaskMonState=0;                   // Monitor task watches Heap and Stack
u_int8  *pubGlobalStackStart;                // Stack pointer on main() entry
#endif   //FEATURE_MONITOR_STACK

//
// Define some steppers and the pins the will use
//
AccelStepper  *ppclAxis       [NUM_AXIS]  = { NULL, NULL, NULL };
const char    *pcAxisName     [NUM_AXIS]  = { "X", "Y", "Z" };
bool           pfAxisRunning  [NUM_AXIS]  = { FALSE, FALSE, FALSE };
//
long           plAxisCurPos   [NUM_AXIS]  = { 0,    0,    0   };
long           plAxisNewPos   [NUM_AXIS]  = { 200,  400,  800 };
float          flSpeedMin     [NUM_AXIS]  = { 400,  400,  400 };
float          flSpeedMax     [NUM_AXIS]  = { 1000, 1500, 800 };
float          flAccelSet     [NUM_AXIS]  = { 0,    0,    0   };
//
// Strings located in ROM
//
static const char *pcLogonMsg     = "CncDraw-" VERSION_STRING CRLF CRLF;
//
u_int8   ubTaskMainState = 0;                   // Main task (time keeping)
u_int8   ubTaskLedsState = 0;                   // Leds task (heartbeat)
u_int8   ubTaskKeysState = 0;                   // Keys task (3 input switches)
//
//-----------------------------------------------------------------------------
// Timer table 10 msecs
//-----------------------------------------------------------------------------
static SCHED stTask10ms[] =
{
};

//-----------------------------------------------------------------------------
// Timer table 100 msecs
//-----------------------------------------------------------------------------
static SCHED stTask100ms[] =
{
   { &ubTaskLedsState, TASK_Leds    },
   { &ubTaskKeysState, TASK_Keys    },
};

//-----------------------------------------------------------------------------
// Timer table 1000 msecs
//-----------------------------------------------------------------------------
static SCHED stTask1000ms[] =
{
   { &ubTaskMainState, TASK_Main    },
#ifdef   FEATURE_MONITOR_STACK
   { &ubTaskMonState,  task_monitor }
#endif   //FEATURE_MONITOR_STACK
};

//
//  Function:  main
//  Purpose:    
//  Parms:      
//
//  Returns:    
//
int main(void)
{
   int         x=0;
   u_int32     ulX=0;
   u_int16     usMsecs=10;          // 100 msecs table
   u_int16     usSecs=100;          // 1   secs  table
   //
   bool        fIdle;
   u_int32     ulIdleTicks;
   u_int32     ulIdleTicksLast=0;
   SCHED      *pstTaskList;
   PTRFUNC     pfAddr;
   u_int8     *pubParm;


#ifdef   FEATURE_MONITOR_STACK
   u_int8      ubStackTop=0xaa;
   //
   // Mark Stack top
   //
   pubGlobalStackStart = &ubStackTop;
#endif   //FEATURE_MONITOR_STACK

   //
   //Init
   //
   stdio_init_all();
   pico_SetupIo();
   pico_SetupUsb();
//   board_init();
//   tusb_init();
   //
   // All IO and USB have been setup
   // Timerticks are 10 msecs
   //
   TIMER_WaitTicks(TRUE, NULL, 500);
   //
   PRINTF(pcLogonMsg);

#ifdef   FEATURE_MONITOR_STACK
   PRINTF1("MAIN:Stack top=%p" CRLF, pubGlobalStackStart);
#endif   //FEATURE_MONITOR_STACK

   //
   // Instantiate Steppers
   // Position all 3 axis
   //
   PRINTF("MAIN:Init positions X-Y-Z" CRLF);
   ppclAxis[AXIS_X] = new AccelStepper(AccelStepper::DRIVER, CNC_X_STP, CNC_X_DIR, CNC_NO_PIN, CNC_NO_PIN);
   ppclAxis[AXIS_Y] = new AccelStepper(AccelStepper::DRIVER, CNC_Y_STP, CNC_Y_DIR, CNC_NO_PIN, CNC_NO_PIN);
   ppclAxis[AXIS_Z] = new AccelStepper(AccelStepper::DRIVER, CNC_Z_STP, CNC_Z_DIR, CNC_NO_PIN, CNC_NO_PIN);
   //
   for(x=0; x<NUM_AXIS; x++)
   {
      pico_SetupCnc(x);
      pfAxisRunning[x] = ppclAxis[x]->moveTo(plAxisNewPos[x]);
   }
   ulX = TIMER_GetTicks();
   PRINTF("Scheduler():Running." CRLF);
   //
   while(1)
   {
      if(ulX != TIMER_GetTicks() )
      {
         ulX = TIMER_GetTicks();
         //--------------------------------------------------------------------------
         // Run the timer "task" lists:
         // Timer list 10 msecs
         //--------------------------------------------------------------------------
         pstTaskList=stTask10ms;
         for(x=0; x<sizeof(stTask10ms)/sizeof(SCHED); x++)
         {
            pfAddr   = pstTaskList->pfFunc;
            pubParm  = pstTaskList->pubState;
         
            *pubParm = pfAddr(*pubParm);
         
            pstTaskList++;
         }
         if(--usMsecs == 0)
         {
            //--------------------------------------------------------------------------
            // Run the timer "task" lists:
            // Timer list 100 msecs
            //--------------------------------------------------------------------------
            pstTaskList=stTask100ms;
            for(x=0; x<sizeof(stTask100ms)/sizeof(SCHED); x++)
            {
               pfAddr   = pstTaskList->pfFunc;
               pubParm  = pstTaskList->pubState;

               *pubParm = pfAddr(*pubParm);
         
               pstTaskList++;
            }
            usMsecs = 10;
         }
         //
         if(--usSecs == 0)
         {
            //--------------------------------------------------------------------------
            // Run the timer "task" lists:
            // Timer list 1000 msecs
            //--------------------------------------------------------------------------
            pstTaskList=stTask1000ms;
            for(x=0; x<sizeof(stTask1000ms)/sizeof(SCHED); x++)
            {
               pfAddr   = pstTaskList->pfFunc;
               pubParm  = pstTaskList->pubState;
               //
               *pubParm = pfAddr(*pubParm);
         
               pstTaskList++;
            }
            usSecs = 100;
         }
      }
      //
      // Run the Steppers the remainder of the time slice. 
      //
      fIdle = TRUE;
      while(fIdle)
      {
         ulIdleTicks = TIMER_GetTicks();
         if(ulIdleTicks != ulIdleTicksLast)
         {
            //
            // New timer tick: loop through the task list
            //
            ulIdleTicksLast = ulIdleTicks;
            fIdle           = FALSE;
         }
         else
         {
            for(x=0; x<NUM_AXIS; x++)
            {
               ppclAxis[x]->run();
               if(ppclAxis[x]->isRunning())
               {
                  if(pfAxisRunning[x] == FALSE)
                  {
                     PRINTF("MAIN:%s is Running to new position" CRLF, pcAxisName[x]);
                     pfAxisRunning[x] = TRUE;
                  }
               }    
               else
               {
                  if(pfAxisRunning[x] == TRUE)
                  {
                     pfAxisRunning[x] = FALSE;
                     plAxisCurPos[x]  = ppclAxis[x]->currentPosition();
                     PRINTF("MAIN:%s has Stopped at %ld" CRLF, pcAxisName[x], plAxisCurPos[x]);
                     if(plAxisCurPos[x]) ppclAxis[x]->moveTo(0);
                     else                ppclAxis[x]->moveTo(plAxisNewPos[x]);
                  }
               }
            }
         }
      }
   }
}

/* ====== Functions separator ===========================================
______________LOCAL_FUNCTIONS(){}
============================X============================================*/


//
//  Function:   pico_SetupCnc
//  Purpose:    init the CNC motors
//  Parms:      Stepper
//
//  Returns:    
//
static void pico_SetupCnc(int x)
{  
   ppclAxis[x]->setPinsInverted(FALSE, FALSE, FALSE);
   ppclAxis[x]->setMinPulseWidth(DEF_PULSE_WIDTH);
   ppclAxis[x]->setSpeed(flSpeedMin[x]);
   ppclAxis[x]->setMaxSpeed(flSpeedMax[x]);
   ppclAxis[x]->setAcceleration(flAccelSet[x]);
   ppclAxis[x]->setCurrentPosition(0);
}

//
//  Function:   pico_SetupIo
//  Purpose:    init the IO
//  Parms:      
//
//  Returns:    
//
static void pico_SetupIo(void)
{
   //
   // Setup Timer: 10 ms heartbeat
   //
   TIMER_Init(10000);    
   //
   // CNC Stepper IO
   //
   gpio_init(GPIO_XAX_STP);
   gpio_init(GPIO_XAX_DIR);
   gpio_init(GPIO_XAX_LM1);
   gpio_init(GPIO_XAX_LM2);
   //
   gpio_init(GPIO_YAX_STP);
   gpio_init(GPIO_YAX_DIR);
   gpio_init(GPIO_YAX_LM1);
   gpio_init(GPIO_YAX_LM2);
   //
   gpio_init(GPIO_ZAX_STP);
   gpio_init(GPIO_ZAX_DIR);
   gpio_init(GPIO_ZAX_LM1);
   gpio_init(GPIO_ZAX_LM2);
   //
}

//
//  Function:   pico_SetupUsb
//  Purpose:    init the USB port
//  Parms:      
//
//  Returns:    
//
static void pico_SetupUsb(void)
{
   //
   // initialize the USB
   //
}

/* ====== Functions separator ===========================================
______________DEBUG_FUNCTIONS(){}
============================X============================================*/

#ifdef   FEATURE_MONITOR_STACK
//
//  Function:   task_monitor
//  Purpose:    monitor task state handler
//  Parms:      state
//
//  Returns:    new state
//
static u_int8 task_monitor(u_int8 ubState)
{
   static u_int8 ubCount;
   //
   u_int8  *pubHeapStart;
   u_int8  *pubHeapCurrent;
   u_int16 usHeapUsed;
   u_int16 usStackUsed;
   
   switch(ubState)
   {
      case 0:
         pubHeapStart = (u_int8*)*(u_int8*)(__malloc_heap_start);
         //
         // Check heap and stack usage
         //
         pubHeapCurrent = (u_int8 *) malloc(4);
         usHeapUsed     = (u_int16) pubHeapCurrent      - (u_int16) pubHeapStart;
         usStackUsed    = (u_int16) pubGlobalStackStart - (u_int16) &pubHeapStart;
         //
         PRINTF1("MAIN:Heap  used=%p" CRLF, usHeapUsed);
         PRINTF1("MAIN:Stack used=0x%x" CRLF, usStackUsed);
         free(pubHeapCurrent);
         //
         ubCount = 10;
         ubState++;
         break;

      default:
      case 1:
         if(ubCount-- == 0) ubState = 0;
         break;
   }
   return(ubState);
}
#endif   //FEATURE_MONITOR_STACK

