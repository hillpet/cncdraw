/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           task_led.h
 *  Purpose:            RPi-Pico task_led header file
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _TASK_LED_H_
#define _TASK_LED_H_

u_int8   TASK_Leds            (u_int8);

#endif  // _TASK_LED_H_
