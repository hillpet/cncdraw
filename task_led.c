/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           task_led.c
 *  Purpose:            RPi-Pico task
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pico/stdlib.h"
//
#include "typedefs.h"
#include "config.h"
#include "globals.h"
#include "timer.h"
#include "bspx.h"
#include "task_led.h"
//
//#define USE_PRINTF
#include "printx.h"

//
// Local data and prototypes
// 

//
//  Function:  TASK_Leds
//  Purpose:   Drive heartbeat led
//  Parms:     State 
//
//  Returns:   New state 
//
u_int8 TASK_Leds(u_int8 ubState)
{
   switch(ubState)
   {
      default:
      case 0:
         gpio_init(GPIO_LED_HBT);
         gpio_set_dir(GPIO_LED_HBT, GPIO_OUT);
         ubState = 1;
         break;

      case 1:
         PRINTF("TASK_Leds():Off" CRLF);
         gpio_put(GPIO_LED_HBT, 0);
         ubState = 2;
         break;

      case 2:
         PRINTF("TASK_Leds():On" CRLF);
         gpio_put(GPIO_LED_HBT, 1);
         ubState = 1;
         break;
   }
   return(ubState);
}

