/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           task_key.h
 *  Purpose:            RPi-Pico task_key header file
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _TASK_KEY_H_
#define _TASK_KEY_H_

EXTERN u_int8  ubKey0;
EXTERN u_int8  ubKey1;
EXTERN u_int8  ubKey2;
//
u_int8   TASK_Keys            (u_int8);

#endif  // _TASK_KEY_H_
