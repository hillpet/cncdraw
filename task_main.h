/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           task_main.h
 *  Purpose:            RPi-Pico task_main header file
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _TASK_MAIN_H_
#define _TASK_MAIN_H_

u_int8   TASK_Main        (u_int8);

#endif  // _TASK_MAIN_H_
