/*  (c) Copyright:  2010..2022  Patrn ESS, Confidential Data
 *
 *
 *  Workfile:           arduino.h
 *  Purpose:            Hreader file porting layer for Arduino type AccelStepper.cpp
 *                      Contains all arduino type constants
 *  Compiler/Assembler: NMake gcc build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Apr 2022:      Ported from TeensyCnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef _ARDUINO_H_
#define _ARDUINO_H_

#include "bspx.h"
//
#define  HIGH                 1
#define  LOW                  0
//
#define  OUTPUT               1
#define  INPUT                0

#define  min(a,b)             (a < b ? a : b)
#define  max(a,b)             (a > b ? a : b)
#define  micros()             TIMER_GetMicroSeconds()

float    constrain            (float, float, float);     
void     delayMilliseconds    (u_int16);
void     delayMicroseconds    (u_int16);
void     digitalWrite         (u_int8, u_int8);
void     pinMode              (u_int8, u_int8);
#endif  // _ARDUINO_H_
