/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           timer.h
 *  Purpose:            RPi-Pico timer header file
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _TIMER_H_
#define _TIMER_H_

//
// Prototypes
//
void     TIMER_Init              (int);                           // Timer calls
u_int32  TIMER_GetTicks          (void);                          // Timer ticks
u_int32  TIMER_GetMicroSeconds   (void);                          // System uSecs
bool     TIMER_WaitTicks         (bool, u_int32 *, u_int32);      // Wair ticks

#endif  // _TIMER_H_
