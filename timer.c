/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           timer.c
 *  Purpose:            RPi-Pico task
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pico/stdlib.h"
//
#include "typedefs.h"
#include "config.h"
#include "globals.h"
#include "timer.h"
//
//#define USE_PRINTF
#include "printx.h"

//
// System counter runs in 64 bits MicroSeconds, and will therefor not wrap for 
// the next centuries. System ticks can be selected (default is 10 mSecs).
//
static int iGranularity = 10000;

/* ====== Functions separator ===========================================
void ____functions____(){}
=========================================================================*/

//
//  Function:  TIMER_Init
//  Purpose:   initialize the timer
//  Parms:     Timer tick granularity in uSecs
//
//  Returns:   Tick count
//             f.i. Granularity is 10000 uSec, timer ticks are in units of 10 mSecs. 
//
void TIMER_Init(int iGran) 
{
   PRINTF("Timer():Init to %d" CRLF, iGran);
   iGranularity = iGran;
}

//
//  Function:  TIMER_GetTicks
//  Purpose:   read the timerticks
//  Parms:     
//
//  Returns:   Timer ticks in 10 msecs increments
//
u_int32 TIMER_GetTicks(void) 
{
   u_int32           ulTick;
   absolute_time_t   tTimeMicroSecs;

   tTimeMicroSecs = get_absolute_time();
   ulTick = (u_int32) (tTimeMicroSecs / iGranularity);

   return(ulTick);
}

//
//  Function:  TIMER_GetMicroSeconds
//  Purpose:   Read the system timer uSecs
//  Parms:     
//
//  Returns:   Timer uSecs
//
u_int32 TIMER_GetMicroSeconds(void) 
{
   absolute_time_t   tTimeMicroSecs;

   tTimeMicroSecs = get_absolute_time();
   return( (u_int32) tTimeMicroSecs);
}

//
//  Function:  TIMER_WaitTicks
//  Purpose:   Delay time
//  Parms:     fWait, Set^, Timeout ticks
//
//  Returns:   fCC=TRUE if time out reached
//
//  Usage:     fWait:      The call will not return until timeout. Else the function will
//                         return with fCC=FALSE.
//             pulSet:     The user supplied tick setpoint.
//             ulTimeout:  The number of ticks to wait
//             Call     :  TIMER_WaitTicks(TRUE, &ulSet, 10);
//                         or
//                         ulSet=0;
//                         while( TIMER_WaitTicks(FALSE, &ulSet, 10) == FALSE)
//                         {
//                          ... // do other things
//                         }
//  Note:                  if fWait==TRUE, there is no need for the ulSet=0.
//                         if pulSet == NULL, we will always wait till timeout.
//
bool TIMER_WaitTicks(bool fWait, u_int32 *pulSet, u_int32 ulTimeout)
{
   bool     fCc=FALSE;
   u_int32  ulTicks, ulSet;

   ulTicks = TIMER_GetTicks();
   //
   if(pulSet)  ulSet = *pulSet;
   else
   {
      ulSet = 0;
      fWait = TRUE;
   }
   //
   if( (ulSet == 0) || fWait )
   {
      //
      // Setup Set value first time only
      //
      ulSet = ulTicks + ulTimeout;
      if(pulSet) *pulSet = ulSet;
   }
   //
   do
   {
      ulTicks = TIMER_GetTicks();
      //
      // Check if time out
      //
      // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      // | | | | | | | | |T-------->S| | | | | | | | | | | | | | | |
      // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      //
      if(ulTicks >= ulSet)
      {
         fCc   = TRUE;
         fWait = FALSE;
      }
   }
   while(fWait);
   //
   return(fCc);
}


