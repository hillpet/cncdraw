/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           config.h
 *  Purpose:            RPi-Pico config file
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//
// Define platform
//
//#define   __RPI_PICO_RP2040__
#define  __MAKER_PI_RP2040__
//
// Define all FEATURE switches here:
//
//    FEATURE_USE_PRINTF         : Global PRINTF enable
//    FEATURE_MONITOR_STACK      : Monitor task list stack usage 
//    FEATURE_CNC_DRIVER         : Directly assume stemmermotor driver H/W (Step+Dir)
//
   #define  FEATURE_USE_PRINTF
// #define  FEATURE_MONITOR_STACK
   #define  FEATURE_CNC_DRIVER
//
#define  DEF_PULSE_WIDTH         100
#define  DEF_ACCEL               100.0
#define  DEF_SPEED_MIN           400.0
#define  DEF_SPEED_MAX           800.0
//
// System specific settings
// Misc defines
//
#define  VERSION_STRING       "v100.005"
typedef enum AXCNC
{
   AXIS_X   = 0,
   AXIS_Y,
   AXIS_Z,
   //
   NUM_AXIS
}  AXCNC;
//
// IO pin definitions
//
typedef enum IOCNC
{
   CNC_NO_PIN  = 0,
   CNC_Z_STP,
   CNC_Z_DIR,
   CNC_X_STP,
   CNC_X_DIR,
   CNC_Y_STP,
   CNC_Y_DIR,
   //
   CNC_NUM_IOS
}  IOCNC;

#endif  // _CONFIG_H_
