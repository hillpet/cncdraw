/*  (c) Copyright:  2010..2022  Patrn ESS, Confidential Data
 *
 *
 *  Workfile:           arduino.c
 *  Purpose:            Porting layer for Arduino type AccelStepper.cpp
 *  Compiler/Assembler: NMake gcc build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Apr 2022:      Ported from TeensyCnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pico/stdlib.h"
//
#include "typedefs.h"
#include "config.h"
#include "globals.h"
#include "timer.h"
#include "arduino.h"
//
//#define USE_PRINTF
#include "printx.h"


static u_int8  ubStateN;
static u_int8  ubStateX;
static u_int8  ubStateY;
static u_int8  ubStateZ;
//
static u_int8  cnc_NoPinMode           (u_int8);
static u_int8  cnc_NoPinIo             (u_int8);
//
static u_int8  cnc_XaxisModeStep       (u_int8);
static u_int8  cnc_XaxisModeDir        (u_int8);
static u_int8  cnc_YaxisModeStep       (u_int8);
static u_int8  cnc_YaxisModeDir        (u_int8);
static u_int8  cnc_ZaxisModeStep       (u_int8);
static u_int8  cnc_ZaxisModeDir        (u_int8);
//
static u_int8  cnc_XaxisIoStep         (u_int8);
static u_int8  cnc_XaxisIoDir          (u_int8);
static u_int8  cnc_YaxisIoStep         (u_int8);
static u_int8  cnc_YaxisIoDir          (u_int8);
static u_int8  cnc_ZaxisIoStep         (u_int8);
static u_int8  cnc_ZaxisIoDir          (u_int8);

static CNC stCncIoList[] =
{
//    pubState    pfMode               pfInOut
   { &ubStateN,   cnc_NoPinMode,       cnc_NoPinIo          },    // CNC_NO_PIN
   { &ubStateX,   cnc_XaxisModeStep,   cnc_XaxisIoStep      },    // CNC_X_STP
   { &ubStateX,   cnc_XaxisModeDir,    cnc_XaxisIoDir       },    // CNC_X_DIR
   { &ubStateY,   cnc_YaxisModeStep,   cnc_YaxisIoStep      },    // CNC_Y_STP
   { &ubStateY,   cnc_YaxisModeDir,    cnc_YaxisIoDir       },    // CNC_Y_DIR
   { &ubStateZ,   cnc_ZaxisModeStep,   cnc_ZaxisIoStep      },    // CNC_Z_STP
   { &ubStateZ,   cnc_ZaxisModeDir,    cnc_ZaxisIoDir       }     // CNC_Z_DIR
};

//
//  Function:   constrain
//  Purpose:    Keep a value between 2 limits
//
//  Parms:      Value, lower limit, upper limit
//  Returns:    Value
//
float constrain(float fVal, float fMin, float fMax)
{
   if     (fVal < fMin)    return(fMin);
   else if(fVal > fMax)    return(fMax);
   else                    return(fVal);
}

//
//  Function:   delayMicroseconds
//  Purpose:    Delay a min number of uSecs
//
//  Parms:      uSecs
//  Returns:    
//
void delayMicroseconds(u_int16 usDelay)
{
   u_int32 ulEnd, ulNow;
   
   if(usDelay)
   {
      ulEnd = TIMER_GetMicroSeconds() + (u_int32) usDelay;
      //
      do
      {
         ulNow = TIMER_GetMicroSeconds();
      }
      while(ulNow < ulEnd);
   }
}

//
//  Function:   delayMilliseconds
//  Purpose:    Delay a min number of mSecs
//
//  Parms:      Secs
//  Returns:    
//
void delayMilliseconds(u_int16 usDelay)
{
   u_int32 ulEnd, ulNow;
   
   if(usDelay)
   {
      ulEnd = TIMER_GetMicroSeconds() + (u_int32) (usDelay*1000);
      //
      do
      {
         ulNow = TIMER_GetMicroSeconds();
      }
      while(ulNow < ulEnd);
   }
}

//
//  Function:   digitalWrite
//  Purpose:    Write a digital value to a I/O-pin
//
//  Parms:      Pin number, value
//  Returns:    
//
void digitalWrite(u_int8 ubPin, u_int8 ubValue)
{
   PTRFUNC     pfInOut;
   u_int8     *pubParm;

   PRINTF("digitalWrite():%d" CRLF, ubPin);
   if( ubPin < sizeof(stCncIoList)/sizeof(CNC) )
   {
      pfInOut = stCncIoList[ubPin].pfInOut;
      pfInOut(ubValue);
   }
   else
   {
      PRINTF("digitalWrite():ERROR pin %d" CRLF, ubPin);
   }
}

//
//  Function:   pinMode
//  Purpose:    Set pinmode
//
//  Parms:      Pin number, mode (INPUT, OUTPUT, ...)
//  Returns:    
//
void pinMode(u_int8 ubPin, u_int8 ubMode)
{
   PTRFUNC     pfMode;
   u_int8     *pubParm;

   PRINTF("pinMode():%d" CRLF, ubPin);
   //
   if( ubPin < sizeof(stCncIoList)/sizeof(CNC) )
   {
      pfMode = stCncIoList[ubPin].pfMode;
      pfMode(ubMode);
   }
   else
   {
      PRINTF("pinMode():ERROR pin %d" CRLF, ubPin);
   }
}

/* ====== Functions separator ===========================================
void ____local_functions____(){}
=========================================================================*/
//
//  Function:   cnc_NoPinMode
//  Purpose:    Handle CNC No pin assigned Direction 
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_NoPinMode(u_int8 ubValue)
{
   return(ubValue);
};

//
//  Function:   cnc_NoPinIo
//  Purpose:    Handle CNC No pin assigned I/O
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_NoPinIo(u_int8 ubValue)
{
   return(ubValue);
};


//
//  Function:   cnc_XaxisModeStep
//  Purpose:    Handle CNC X axis Mode for step pulse
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_XaxisModeStep(u_int8 ubValue)
{
   PRINTF("cnc-XaxisModeStep():Xms=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_XAX_MODE_STP(1);
   else        CNC_XAX_MODE_STP(0);
   return(ubValue);
};

//
//  Function:   cnc_XaxisModeDir
//  Purpose:    Handle CNC X axis Mode for direction
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_XaxisModeDir (u_int8 ubValue)
{
   PRINTF("cnc-XaxisModeDir():Xmd=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_XAX_MODE_DIR(1);
   else        CNC_XAX_MODE_DIR(0);
   return(ubValue);
};

//
//  Function:   cnc_YaxisModeStep
//  Purpose:    Handle CNC Y axis Mode for step pulse
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_YaxisModeStep(u_int8 ubValue)
{
   PRINTF("cnc-YaxisModeStep():Yms=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_YAX_MODE_STP(1);
   else        CNC_YAX_MODE_STP(0);
   return(ubValue);
};

//
//  Function:   cnc_YaxisModeDir
//  Purpose:    Handle CNC Y axis Mode for direction
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_YaxisModeDir(u_int8 ubValue)
{
   PRINTF("cnc-YaxisModeDir():Ymd=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_YAX_MODE_DIR(1);
   else        CNC_YAX_MODE_DIR(0);
   return(ubValue);
};

//
//  Function:   cnc_ZaxisModeStep
//  Purpose:    Handle CNC Z axis Mode for step pulse
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_ZaxisModeStep(u_int8 ubValue)
{
   PRINTF("cnc-ZaxisModeStep():Zms=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_ZAX_MODE_STP(1);
   else        CNC_ZAX_MODE_STP(0);
   return(ubValue);
};

//
//  Function:   cnc_ZaxisModeDir
//  Purpose:    Handle CNC Z axis Mode for direction
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_ZaxisModeDir(u_int8 ubValue)
{
   PRINTF("cnc-ZaxisModeDir():Zmd=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_ZAX_MODE_DIR(1);
   else        CNC_ZAX_MODE_DIR(0);
   return(ubValue);
};

//
//  Function:   cnc_XaxisIoStep
//  Purpose:    X axis step
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_XaxisIoStep(u_int8 ubValue)
{
   PRINTF("cnc-XaxisIoStep():Xs=%d" CRLF, ubValue);
   if(ubValue) CNC_XAX_STP_1();
   else        CNC_XAX_STP_0();
   return(ubValue);
};

//
//  Function:   cnc_XaxisIoDir
//  Purpose:    X axis set direction
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_XaxisIoDir(u_int8 ubValue)
{
   PRINTF("cnc-XaxisIoDir():Xd=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_XAX_DIR(1);
   else        CNC_XAX_DIR(0);
   return(ubValue);
};

//
//  Function:   cnc_YaxisIoStep
//  Purpose:    Y axis step
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_YaxisIoStep(u_int8 ubValue)
{
   PRINTF("cnc-YaxisIoStep():Ys=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_YAX_STP_1();
   else        CNC_YAX_STP_0();
   return(ubValue);
};

//
//  Function:   cnc_YaxisIoDir
//  Purpose:    Y axis set direction
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_YaxisIoDir(u_int8 ubValue)
{
   PRINTF("cnc-YaxisIoDir():Yd=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_YAX_DIR(1);
   else        CNC_YAX_DIR(0);
   return(ubValue);
};

//
//  Function:   cnc_ZaxisIoStep
//  Purpose:    Z axis step
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_ZaxisIoStep(u_int8 ubValue)
{
   PRINTF("cnc-ZaxisIoStep():Zs=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_ZAX_STP_0();
   else        CNC_ZAX_STP_1();
   return(ubValue);
};

//
//  Function:   cnc_ZaxisIoDir
//  Purpose:    Z axis set direction
//
//  Parms:      Value
//  Returns:    
//
static u_int8 cnc_ZaxisIoDir(u_int8 ubValue)
{
   PRINTF("cnc-ZaxisIoDir():Zd=%d" CRLF, ubValue);
   //
   if(ubValue) CNC_ZAX_DIR(1);
   else        CNC_ZAX_DIR(0);
   return(ubValue);
};




