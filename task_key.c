/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           task_key.c
 *  Purpose:            RPi-Pico task
 *  Compiler/Assembler: CMake/NMake build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pico/stdlib.h"
//
#include "typedefs.h"
#include "config.h"
#include "globals.h"
#include "timer.h"
#include "bspx.h"
#include "task_key.h"
//
//#define USE_PRINTF
#include "printx.h"

//
//  Function:  TASK_Leds
//  Purpose:   Scan input switches
//  Parms:     State 
//
//  Returns:   New state 
//
u_int8 TASK_Keys(u_int8 ubState)
{
   switch(ubState)
   {
      default:
      case 0:
         gpio_init(GPIO_KEY_0);
         gpio_init(GPIO_KEY_1);
         gpio_init(GPIO_KEY_2);
         //
         gpio_set_dir(GPIO_KEY_0, GPIO_IN);
         gpio_set_dir(GPIO_KEY_1, GPIO_IN);
         gpio_set_dir(GPIO_KEY_2, GPIO_IN);
         ubState = 1;
         break;

      case 1:
         if(gpio_get(GPIO_KEY_0)) ubKey0 = 0;
         else                     ubKey0 = 1;
         if(gpio_get(GPIO_KEY_1)) ubKey1 = 0;
         else                     ubKey1 = 1;
         if(gpio_get(GPIO_KEY_2)) ubKey2 = 0;
         else                     ubKey2 = 1;
         break;
   }
   return(ubState);
}

