/*  (c) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           printx.h
 *  Purpose:            Generic printer redirections
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    08 Apr 2022:      Copied from pico-patrn\scheduler
 *    09 Apr 2022:      Use Variadic Macro's (backwards comp)
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef  _PRINTX_H_
#define  _PRINTX_H_

//
// Define CRLF string : PRINTF("This is the text" CRLF);
//
#define CR                    '\r'
#define LF                    '\n'
//
#define NEWLINE               "\r\n"
#define CRLF                  "\r\n"
#define CRLF2                 "\r\n\r\n"
#define TAB                   '\t'
//
#define  ERR_PRINTF(...)               fprintf(stderr, __VA_ARGS__)
//
//  Makefile should specify one of:
//
//    FEATURE_USE_PRINTF   - LOG_printf  trace data to stdout for debug on the spot
//    FEATURE_USE_STDOUT   - printf      trace data to stdout for debug on the spot
//    FEATURE_TRACE_PRINTF - LOG_Trace   trace data to persistent file system for debug lockups which
//                                       require reboots or hard kills which would erase the TMPFS log files.
//
#if defined (USE_PRINTF)

#if defined (FEATURE_USE_PRINTF)
// #warning FEATURE_USE_PRINTF
   #define  PRINTF(...)                printf(__VA_ARGS__)
   #define  UNC_PRINTF(...)            printf(__VA_ARGS__)

#elif defined (FEATURE_LOG_PRINTF)
   #warning FEATURE_LOG_PRINTF
   #define  PRINTF(...)                printf(__VA_ARGS__)
   #define  UNC_PRINTF(...)            printf(__VA_ARGS__)

#elif defined (FEATURE_USE_STDOUT)
   #warning FEATURE_USE_STDOUT
   #define  PRINTF(...)                printf(__VA_ARGS__)
   #define  UNC_PRINTF(...)            printf(__VA_ARGS__)

#elif defined (FEATURE_TRACE_PRINTF)
   #warning FEATURE_TRACE_PRINTF
   #define  PRINTF(...)                printf(__VA_ARGS__)
   #define  UNC_PRINTF(...)            printf(__VA_ARGS__)

#else
// #warning NO FEATURE_PRINTF
   #define  PRINTF(...)
   #define  UNC_PRINTF(...)

#endif

#else
// #warning NO USE_PRINTF
   #define  PRINTF(...)
   #define  UNC_PRINTF(...)

#endif   // USE_PRINTF

#endif   // _PRINTF_X_

