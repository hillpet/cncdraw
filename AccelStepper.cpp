/*  (C) Copyright:  2022  Patrn ESS, Confidential Data
 *
 *  Workfile:           AccelStepper.cpp
 *  Purpose:            Stepper motor control class
 *  Compiler/Assembler: NMake gcc build
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:            
 *    29 Mar 2016:      Merge v1.51 changes
 *                      (C) Copyright:  2009-2013 Mike McCauley
 *                      AccelStepper.cpp, v1.21 2015/08/25 04:57:29 mikem Exp mikem $
 *    09 Apr 2022:      Ported from TeensyCnc
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <cstdint>

extern "C" 
{
   #include <stdio.h>
   #include <string.h>
   #include <stdlib.h>
   #include <math.h>
   //
   #include "pico/stdlib.h"
   //
   #include "typedefs.h"
   #include "config.h"
   #include "globals.h"
   #include "timer.h"
   #include "arduino.h"
   #include "task_main.h"
   #include "task_led.h"
   #include "task_key.h"
   //
   #define USE_PRINTF
   #include "printx.h"
}
#include "AccelStepper.h"

//
//  Function:  AccelStepper
//  Purpose:   Constructor
//  Parms:     Interface, Step pin, Dir pin, Lim- pin, Lim+ pin
//  Returns:   
//
AccelStepper::AccelStepper(uint8_t interface, uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4, bool enable)
{
   int i;

   _interface     = interface;
   _currentPos    = 0;
   _targetPos     = 0;
   _speed         = 0.0;
   _maxSpeed      = 1.0;
   _acceleration  = 0.0;
   _stepInterval  = 0;
   _minPulseWidth = 1;
   _enablePin     = 0xff;
   _lastStepTime  = 0;
   //
   _pin[0]        = pin1;
   _pin[1]        = pin2;
   _pin[2]        = pin3;
   _pin[3]        = pin4;
   //
   _n             = 0;
   _c0            = 0.0;
   _cn            = 0.0;
   _cmin          = 1.0;
   _direction     = DIRECTION_CCW;
   //
   for (i = 0; i < 4; i++) _pinInverted[i] = 0;
   //
   PRINTF("AS::Constructor:Ifc=%d Pins=(%d,%d,%d,%d)" CRLF, interface, pin1, pin2, pin3, pin4);
   if (enable) enableOutputs();
   setAcceleration(1);
}

//
//  Function:  AccelStepper
//  Purpose:   Constructor
//  Parms:     
//
//  Returns:   
//
AccelStepper::AccelStepper(void (*forward)(), void (*backward)())
{
   _interface     = 0;
   _currentPos    = 0;
   _targetPos     = 0;
   _speed         = 0.0;
   _maxSpeed      = 1.0;
   _acceleration  = 0.0;
   _stepInterval  = 0;
   _minPulseWidth = 1;
   _enablePin     = 0xff;
   _lastStepTime  = 0;
   //
   _pin[0]        = 0;
   _pin[1]        = 0;
   _pin[2]        = 0;
   _pin[3]        = 0;
   _forward       = forward;
   _backward      = backward;
   //
   // NEW
   //
   _n             = 0;
   _c0            = 0.0;
   _cn            = 0.0;
   _cmin          = 1.0;
   _direction     = DIRECTION_CCW;
   //
   PRINTF("AS::Constructor" CRLF);
   for (int i = 0; i < 4; i++) _pinInverted[i] = 0;
   setAcceleration(1);
}

//
//  Function:  moveTo
//  Purpose:   Move stepper to fixed position
//  Parms:     Fixed position
//
//  Returns:   InMotion yesno
//
bool AccelStepper::moveTo(long absolute)
{
   PRINTF("AS::moveTo():%ld" CRLF, absolute);
   if (_targetPos != absolute)
   {
      _targetPos = absolute;
      computeNewSpeed();
   }
   return(isRunning());
}

//
//  Function:  move
//  Purpose:   Move stepper to relative position
//  Parms:     Relative position
//
//  Returns:   InMotion yesno
//
bool AccelStepper::move(long relative)
{
   PRINTF("AS::move():%ld" CRLF, relative);
   return(moveTo(_currentPos + relative));
}

//
//  Function:  runSpeed
//  Purpose:   Implements steps according to the current step interval
//             You must call this at least once per step
//             
//  Parms:     
//
//  Returns:   true if a step occurred
//
bool AccelStepper::runSpeed()
{
   //
   // Dont do anything unless we actually have a step interval
   //
   if(!_stepInterval) return(false);

   unsigned long ulCurrTime     = micros();
   unsigned long ulNextStepTime = _lastStepTime + _stepInterval;

   //
   // Gymnastics to detect wrapping of either the nextStepTime and/or the current time
   //
   if ( ((ulNextStepTime >= _lastStepTime) && ((ulCurrTime >= ulNextStepTime) || (ulCurrTime < _lastStepTime)) )
            || 
        ((ulNextStepTime <  _lastStepTime) && ((ulCurrTime >= ulNextStepTime) && (ulCurrTime < _lastStepTime))) )
   {
      if (_direction == DIRECTION_CW)
      {
         // Clockwise
         _currentPos += 1;
      }
      else
      {
         // Anticlockwise  
         _currentPos -= 1;
      }


#ifdef FEATURE_CNC_DRIVER
      step1(_currentPos);
#else
      step(_currentPos);
#endif
      _lastStepTime = ulCurrTime;
      return(true);
   }
   else
   {
      return(false);
   }
}

//
//  Function:  distanceToGo
//  Purpose:   
//  Parms:     
//
//  Returns:   
//
long AccelStepper::distanceToGo()
{
   long  lDist;

   lDist = _targetPos - _currentPos;
   //PRINTF("AS::distanceToGo():%ld" CRLF, lDist);
   return(lDist);
}

//
//  Function:  
//  Purpose:   
//  Parms:     
//
//  Returns:   
//
long AccelStepper::targetPosition()
{
   PRINTF("AS::targetPosition():%ld" CRLF, _targetPos);
   return(_targetPos);
}

//
//  Function:  
//  Purpose:   
//  Parms:     
//
//  Returns:   
//
long AccelStepper::currentPosition()
{
   PRINTF("AS::currentPosition():%ld" CRLF, _currentPos);
   return(_currentPos);
}

//
//  Function:  setCurrentPosition
//  Purpose:   Set the current position
//  Parms:     Position
//
//  Returns:   
//  Note:      Useful during initialisations or after initial positioning
//             Sets speed to 0
//
void AccelStepper::setCurrentPosition(long position)
{
   PRINTF("AS::setCurrentPosition():%ld" CRLF, position);
   _targetPos     = _currentPos = position;
   _n             = 0;
   _stepInterval  = 0;
   _speed         = 0.0;
}

//
//  Function:  computeNewSpeed
//  Purpose:   Compute new variable according new speed
//  Parms:     
//
//  Returns:   
//
void AccelStepper::computeNewSpeed()
{
   long lDistanceTo   = distanceToGo();                                       // +ve is clockwise from curent location
   long lStepsToStop  = (long)((_speed * _speed) / (2.0 * _acceleration));    // Equation 16

   if (lDistanceTo == 0 && lStepsToStop <= 1)
   {
      //
      // We are at the target and its time to stop
      //
      _stepInterval  = 0;
      _speed         = 0.0;
      _n             = 0;
      return;
   }
   if (lDistanceTo > 0)
   {
      //
      // We are anticlockwise from the target
      // Need to go clockwise from here, maybe decelerate now
      //
      if (_n > 0)
      {
         //
         // Currently accelerating, need to decel now? Or maybe going the wrong way?
         //
         if ((lStepsToStop >= lDistanceTo) || _direction == DIRECTION_CCW)
         {
            _n = -lStepsToStop; // Start deceleration
         }
      }
      else if (_n < 0)
      {
         //
         // Currently decelerating, need to accel again?
         //
         if ((lStepsToStop < lDistanceTo) && _direction == DIRECTION_CW) 
         {
            _n = -_n; // Start accceleration
         }
      }
   }
   else if (lDistanceTo < 0)
   {
      //
      // We are clockwise from the target
      // Need to go anticlockwise from here, maybe decelerate
      //
      if (_n > 0)
      {
         //
         // Currently accelerating, need to decel now? Or maybe going the wrong way?
         //
         if ((lStepsToStop >= -lDistanceTo) || _direction == DIRECTION_CW) 
         {
            _n = -lStepsToStop; // Start deceleration
         }
      }
      else if (_n < 0)
      {
         //
         // Currently decelerating, need to accel again?
         //
         if ((lStepsToStop < -lDistanceTo) && _direction == DIRECTION_CCW) 
         {
            //
            // Start accceleration
            //
            _n = -_n; 
         }
      }
   }
   //
   // Need to accelerate or decelerate
   //
   if (_n == 0)
   {
      //
      // First step from stopped
      //
      _cn = _c0;
      _direction = (lDistanceTo > 0) ? DIRECTION_CW : DIRECTION_CCW;
   }
   else
   {
      //
      // Subsequent step. Works for accel (n is +_ve) and decel (n is -ve).
      //
      _cn = _cn - ((2.0 * _cn) / ((4.0 * _n) + 1)); // Equation 13
      _cn = max(_cn, _cmin); 
   }
   //
   _n++;
   _stepInterval = _cn;
   _speed        = 1000000.0 / _cn;
   //
   if (_direction == DIRECTION_CCW) 
   {
      _speed = -_speed;
   }
   //PRINTF("AS::computeNewSpeed():n=%d c0=%f cmin=%f cn=%f Speed=%f Step=%d" CRLF, _n, _c0, _cmin, _cn, _speed, _stepInterval);

#if 0
   Serial.println(_speed);
   Serial.println(_acceleration);
   Serial.println(_cn);
   Serial.println(_c0);
   Serial.println(_n);
   Serial.println(_stepInterval);
   Serial.println(distanceTo);
   Serial.println(stepsToStop);
   Serial.println("-----");
#endif
}

//
//  Function:  run
//  Purpose:   Run the motor to implement speed and acceleration in order to proceed to the target position
//             You must call this at least once per step, preferably in your main loop
//             If the motor is in the desired position, the cost is very small
//             returns true if the motor is still running to the target position.
//  Parms:     
//
//  Returns:   
//
bool AccelStepper::run()
{
   if (runSpeed()) computeNewSpeed();
   return(_speed != 0.0 || distanceToGo() != 0);
}

//
//  Function:  setMaxSpeed
//  Purpose:   Set the maximum speed
//  Parms:     The speed
//  Returns:   
//
void AccelStepper::setMaxSpeed(float speed)
{
   if (_maxSpeed != speed)
   {
      PRINTF("AS::setMaxSpeed():%f" CRLF, speed);
      _maxSpeed = speed;
      _cmin = 1000000.0 / speed;
      //
      // Recompute _n from current speed and adjust speed if accelerating or cruising
      //
      if (_n > 0)
      {
          _n = (long)((_speed * _speed) / (2.0 * _acceleration)); // Equation 16
          computeNewSpeed();
      }
   }
}

//
//  Function:  maxSpeed
//  Purpose:   Return the maximum speed
//  Parms:     
//  Returns:   The max speed
//
float AccelStepper::maxSpeed()
{
   PRINTF("AS::maxSpeed():%f" CRLF, _maxSpeed);
   return(_maxSpeed);
}

//
//  Function:  setAcceleration
//  Purpose:   Set the new acceleration
//  Parms:     The acceleration
//  Returns:   
//
void AccelStepper::setAcceleration(float acceleration)
{
   PRINTF("AS::setAcceleration():%f" CRLF, acceleration);
   //
   if ( acceleration == 0.0) return;
   if (_acceleration != acceleration)
   {
      //
      // Recompute _n per Equation 17
      //
      _n = _n * (_acceleration / acceleration);
      //
      // New c0 per Equation 7, with correction per Equation 15
      //
      _c0 = 0.676 * sqrt(2.0 / acceleration) * 1000000.0; // Equation 15
      _acceleration = acceleration;
      computeNewSpeed();
   }
}

//
//  Function:  setSpeed
//  Purpose:   Set the new speed
//  Parms:     The speed
//  Returns:   
//
void AccelStepper::setSpeed(float speed)
{
   PRINTF("AS::setSpeed():%f" CRLF, speed);
   //
   if (speed == _speed) return;
   //
   speed = constrain(speed, -_maxSpeed, _maxSpeed);
   //
   if (speed == 0.0) _stepInterval = 0;
   else
   {
      _stepInterval = fabs(1000000.0 / speed);
      _direction    = (speed > 0.0) ? DIRECTION_CW : DIRECTION_CCW;
   }
   _speed = speed;
}

//
//  Function:  speed
//  Purpose:   Return current speed
//  Parms:     
//  Returns:   
//
float AccelStepper::speed()
{
   PRINTF("AS::speed():%f" CRLF, _speed);
   //
   return(_speed);
}

//
//  Function:  step
//  Purpose:   Handle the steppermotor step
//             Subclasses can override
//  Parms:     The step 
//  Returns:   
//
void AccelStepper::step(long step)
{
   switch (_interface)
   {
      case FUNCTION:
         //PRINTF("AS::step():FUNCTION %d" CRLF, step);
         step0(step);
         break;

      case DRIVER:
         //PRINTF("AS::step():DRIVER %d" CRLF, step);
         step1(step);
         break;
       
      case FULL2WIRE:
         //PRINTF("AS::step():FULL2WIRE %d" CRLF, step);
         step2(step);
         break;
       
      case FULL3WIRE:
         //PRINTF("AS::step():FULL3WIRE %d" CRLF, step);
         step3(step);
         break;  

      case FULL4WIRE:
         //PRINTF("AS::step():FULL4WIRE %d" CRLF, step);
         step4(step);
         break;  

      case HALF3WIRE:
         //PRINTF("AS::step():HALF3WIRE %d" CRLF, step);
         step6(step);
         break;  
      
      case HALF4WIRE:
         //PRINTF("AS::step():HALF4WIRE %d" CRLF, step);
         step8(step);
         break;  
   }
}

//
//  Function:  setOutputPins
//  Purpose:   
//             You might want to override this to implement eg serial output
//             bit 0 of the mask corresponds to _pin[0]
//             bit 1 of the mask corresponds to _pin[1]
//             ....
//  Parms:     
//  Returns:   
//
//
void AccelStepper::setOutputPins(uint8_t mask)
{
   //PRINTF("AS::setOutputPins():%d" CRLF, mask);

#ifdef FEATURE_CNC_DRIVER
   digitalWrite(_pin[0], (mask & 1) ? (HIGH ^ _pinInverted[0]) : (LOW ^ _pinInverted[0]));
   digitalWrite(_pin[1], (mask & 2) ? (HIGH ^ _pinInverted[1]) : (LOW ^ _pinInverted[1]));
#else
   uint8_t i;
   uint8_t numpins;

   if      (_interface == FULL4WIRE || _interface == HALF4WIRE)   numpins = 4;
   else if (_interface == FULL3WIRE || _interface == HALF3WIRE)   numpins = 3;
   else                                                           numpins = 2;
   //
   for (i=0; i<numpins; i++)
   {
      digitalWrite(_pin[i], (mask & (1 << i)) ? (HIGH ^ _pinInverted[i]) : (LOW ^ _pinInverted[i]));
   }
#endif
}

//
//  Function:  step0
//  Purpose:   0 pin step function (ie for functional usage)
//  Parms:     
//  Returns:   
//
void AccelStepper::step0(long step)
{
   //PRINTF("AS::step0():%ld" CRLF, step);
   //
   if (_speed > 0)   _forward();
   else              _backward();
}

//
//  Function:  step1
//  Purpose:   1 pin step function (ie for stepper drivers)
//             This is passed the current step number (0 to 7)
//             Subclasses can override
//  Parms:     Current step
//  Returns:   
//
void AccelStepper::step1(long step)
{
   //PRINTF("AS::step1():%ld" CRLF, step);
   //
   // _pin[0] is step, _pin[1] is direction
   //
   setOutputPins(_direction ? 0b10 : 0b00); // Set direction first else get rogue pulses
   setOutputPins(_direction ? 0b11 : 0b01); // step HIGH
   //
   // Caution 200ns setup time 
   // Delay the minimum allowed pulse width
   //
   delayMicroseconds(_minPulseWidth);
   setOutputPins(_direction ? 0b10 : 0b00); // step LOW
}


//
//  Function:  step2
//  Purpose:   2 pin half step function
//             This is passed the current step number (0 to 7)
//             Subclasses can override
//  Parms:     Current step
//  Returns:   
//
void AccelStepper::step2(long step)
{
   //PRINTF("AS::step2():%ld" CRLF, step);
   //
   switch (step & 0x3)
   {
      case 0: /* 01 */
         setOutputPins(0b10);
         break;

      case 1: /* 11 */
         setOutputPins(0b11);
         break;

      case 2: /* 10 */
         setOutputPins(0b01);
         break;

      case 3: /* 00 */
         setOutputPins(0b00);
         break;
    }
}
//
//  Function:  step3
//  Purpose:   3 pin half step function
//             This is passed the current step number (0 to 7)
//             Subclasses can override
//  Parms:     Current step
//  Returns:   
//
void AccelStepper::step3(long step)
{
   //PRINTF("AS::step3():%ld" CRLF, step);
   //
   switch (step % 3)
   {
      case 0:    // 100
         setOutputPins(0b100);
         break;

      case 1:    // 001
         setOutputPins(0b001);
         break;

      case 2:    //010
         setOutputPins(0b010);
         break;
    }
}

//
//  Function:  step4
//  Purpose:   4 pin half step function
//             This is passed the current step number (0 to 7)
//             Subclasses can override
//  Parms:     Current step
//  Returns:   
//
void AccelStepper::step4(long step)
{
   //PRINTF("AS::step4():%ld" CRLF, step);
   //
   switch (step & 0x3)
   {
      case 0:    // 1010
         setOutputPins(0b0101);
         break;

      case 1:    // 0110
         setOutputPins(0b0110);
         break;

      case 2:    //0101
         setOutputPins(0b1010);
         break;

      case 3:    //1001
         setOutputPins(0b1001);
         break;
   }
}

//
//  Function:  step6
//  Purpose:   3 pin half step function
//             This is passed the current step number (0 to 7)
//             Subclasses can override
//  Parms:     Current step
//  Returns:   
//
void AccelStepper::step6(long step)
{
   //PRINTF("AS::step6():%ld" CRLF, step);
   //
   switch (step % 6)
   {
      case 0:    // 100
         setOutputPins(0b100);
         break;
          
      case 1:    // 101
         setOutputPins(0b101);
         break;
          
      case 2:    // 001
         setOutputPins(0b001);
         break;
          
      case 3:    // 011
         setOutputPins(0b011);
         break;
          
      case 4:    // 010
         setOutputPins(0b010);
         break;
          
      case 5:    // 011
         setOutputPins(0b110);
         break;
   }
}

//
//  Function:  step8
//  Purpose:   4 pin half step function
//             This is passed the current step number (0 to 7)
//             Subclasses can override
//  Parms:     Current step
//  Returns:   
//
void AccelStepper::step8(long step)
{
   //PRINTF("AS::step8():%ld" CRLF, step);
   //
   switch (step & 0x7)
   {
      case 0:    // 1000
         setOutputPins(0b0001);
         break;
          
      case 1:    // 1010
         setOutputPins(0b0101);
         break;
          
      case 2:    // 0010
         setOutputPins(0b0100);
         break;
          
      case 3:    // 0110
         setOutputPins(0b0110);
         break;
          
      case 4:    // 0100
         setOutputPins(0b0010);
         break;
          
      case 5:    //0101
         setOutputPins(0b1010);
         break;
          
      case 6:    // 0001
         setOutputPins(0b1000);
         break;
          
      case 7:    //1001
         setOutputPins(0b1001);
         break;
   }
}
    
//
//  Function:  disableOutputs
//  Purpose:   Disable the outputs to free the motor
//             Prevents power consumption on the outputs
//  Parms:     
//  Returns:   
//
void AccelStepper::disableOutputs()
{   
   PRINTF("AS::disableOutputs()" CRLF);
   //
   if (! _interface) return;

   setOutputPins(0);                                     // Handles inversion automatically
   if (_enablePin != 0xff) 
   {
      pinMode(_enablePin, OUTPUT);
      digitalWrite(_enablePin, LOW ^ _enableInverted);
   }
}

//
//  Function:  enableOutputs
//  Purpose:   Enable the outputs to hold the motor
//  Parms:     
//  Returns:   
//
void AccelStepper::enableOutputs()
{
   PRINTF("AS::enableOutputs()" CRLF);
   //
   if (!_interface) return;

   pinMode(_pin[0], OUTPUT);
   pinMode(_pin[1], OUTPUT);
   if (_interface == FULL4WIRE || _interface == HALF4WIRE)
   {
       pinMode(_pin[2], OUTPUT);
       pinMode(_pin[3], OUTPUT);
   }
   else if (_interface == FULL3WIRE || _interface == HALF3WIRE)
   {
       pinMode(_pin[2], OUTPUT);
   }
   if (_enablePin != 0xff)
   {
       pinMode(_enablePin, OUTPUT);
       digitalWrite(_enablePin, HIGH ^ _enableInverted);
   }
}

//
//  Function:  setMinPulseWidth
//  Purpose:   Set the minimum steppermotor pulse width
//  Parms:     Min pulsewidth
//  Returns:   
//
void AccelStepper::setMinPulseWidth(unsigned int iMinWidth)
{
   PRINTF("AS::setMinPulseWidth():%d" CRLF, iMinWidth);
   //
   _minPulseWidth = iMinWidth;
}

//
//  Function:  setEnablePin
//  Purpose:   
//  Parms:     
//  Returns:   
//
void AccelStepper::setEnablePin(uint8_t enablePin)
{
   PRINTF("AS::setEnablePin():%d" CRLF, enablePin);
   //
   _enablePin = enablePin;
   //
   // This happens after construction, so init pin now.
   //
   if (_enablePin != 0xff)
   {
      pinMode(_enablePin, OUTPUT);
      digitalWrite(_enablePin, HIGH ^ _enableInverted);
   }
}

//
//  Function:  setPinsInverted
//  Purpose:   
//  Parms:     
//  Returns:   
//
void AccelStepper::setPinsInverted(bool directionInvert, bool stepInvert, bool enableInvert)
{
   PRINTF("AS::setPinsInverted():Stp=%d, Dir=%d, Inv=%d" CRLF, directionInvert, stepInvert, enableInvert);
   //
   _pinInverted[0] = stepInvert;
   _pinInverted[1] = directionInvert;
   _enableInverted = enableInvert;
}

//
//  Function:  setPinsInverted
//  Purpose:   
//  Parms:     
//  Returns:   
//
void AccelStepper::setPinsInverted(bool pin1Invert, bool pin2Invert, bool pin3Invert, bool pin4Invert, bool enableInvert)
{    
   PRINTF("AS::setPinsInverted():Pins=(%d,%d,%d,%d) Inv=%d" CRLF, pin1Invert, pin2Invert, pin3Invert, pin4Invert, enableInvert);
   //
   _pinInverted[0] = pin1Invert;
   _pinInverted[1] = pin2Invert;
   _pinInverted[2] = pin3Invert;
   _pinInverted[3] = pin4Invert;
   _enableInverted = enableInvert;
}

//
//  Function:  runToPosition
//  Purpose:   Go to set position
//             Blocks until the target position is reached and stopped
//  Parms:     
//  Returns:   
//
void AccelStepper::runToPosition()
{
   PRINTF("AS::runToPosition()" CRLF);
   //
   while( run() )
   ;
}

//
//  Function:  runSpeedToPosition
//  Purpose:   Go to the new set position
//             Implements steps according to the current step interval
//             You must call this at least once per step
//  Parms:     
//  Returns:   true if still running
//
bool AccelStepper::runSpeedToPosition()
{
   //PRINTF("AS::runSpeedToPosition():From %ld to %ld" CRLF, _currentPos, _targetPos);
   //
   if (_targetPos == _currentPos) return false;
   //
   if (_targetPos >_currentPos)   _direction = DIRECTION_CW;
   else                           _direction = DIRECTION_CCW;
   return(runSpeed());
}

//
//  Function:  runToNewPosition
//  Purpose:   Run steppermotor to a new position
//             Blocks until the new target position is reached
//  Parms:     New position
//
//  Returns:   
//
void AccelStepper::runToNewPosition(long lNewPosition)
{
   PRINTF("AS::runToNewPosition():%ld" CRLF, lNewPosition);
   //
   moveTo(lNewPosition);
   runToPosition();
}

//
//  Function:  stop
//  Purpose:   Have this stepper perform a controlled stop
//  Parms:     
//  Returns:   
//
void AccelStepper::stop()
{
   PRINTF("AS::stop()" CRLF);
   //
   if (_speed != 0.0)
   {    
      long lStepsToStop = (long)((_speed * _speed) / (2.0 * _acceleration)) + 1; // Equation 16 (+integer rounding)
      if (_speed > 0) move( lStepsToStop);
      else            move(-lStepsToStop);
   }
}

//
//  Function:  isRunning
//  Purpose:   Check if this stepper is still running
//  Parms:     
//  Returns:   true if running
//
bool AccelStepper::isRunning()
{
   return(!( (_speed == 0.0) && (_targetPos == _currentPos)));
}

